<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class User_controller extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $title = 'Data User';
    $data = \DB::table('users')->get();

    return view('admin.user.user_index',compact('title','data'));
  }

  public function add(){
    $title = 'tambah user';

    return view('admin.user.user_add',compact('title'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    \DB::table('users')->insert([
      'name'=>$request->name,
      'email'=>$request->email,
      'password'=>bcrypt($request->password),
      'created_at'=>date('Y-m-d H:i:s')
    ]);

    return redirect('admin/user');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $title = 'Edit User';
    $dt = \DB::table('users')->where('id',$id)->first();

    return view('admin.user.user_edit',compact('dt','title'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    \DB::table('users')->where('id',$id)->update([
      'name'=>$request->name,
      'email'=>$request->email,
      'password'=>bcrypt($request->password),
      'updated_at'=>date('Y-m-d H:i:s')
    ]);

    return redirect('admin/user');
  }

  public function delete($id){
    \DB::table('users')->where('id',$id)->delete();

    return redirect('admin/user');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
