<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="A slideshow component for cycling through elements—images or slides of text—like a carousel.">
  <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
  <meta name="generator" content="Jekyll v3.7.0">

  <title>Purbalinggaku</title>

  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/components/carousel/">

  <!-- Bootstrap core CSS -->

  <style class="anchorjs"></style><link href="/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


  <!-- Documentation extras -->

  <link href="https://cdn.jsdelivr.net/npm/docsearch.js@2/dist/cdn/docsearch.min.css" rel="stylesheet">

  <link href="/docs/4.0/assets/css/docs.min.css" rel="stylesheet">

  <!-- Favicons -->
  <link rel="apple-touch-icon" href="/docs/4.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
  <link rel="manifest" href="/docs/4.0/assets/img/favicons/manifest.json">
  <link rel="mask-icon" href="/docs/4.0/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
  <meta name="msapplication-config" content="/docs/4.0/assets/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#563d7c">


  <!-- Twitter -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@getbootstrap">
  <meta name="twitter:creator" content="@getbootstrap">
  <meta name="twitter:title" content="Carousel">
  <meta name="twitter:description" content="A slideshow component for cycling through elements—images or slides of text—like a carousel.">
  <meta name="twitter:image" content="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-social-logo.png">

  <!-- Facebook -->
  <meta property="og:url" content="https://getbootstrap.com/docs/4.0/components/carousel/">
  <meta property="og:title" content="Carousel">
  <meta property="og:description" content="A slideshow component for cycling through elements—images or slides of text—like a carousel.">
  <meta property="og:type" content="website">
  <meta property="og:image" content="http://getbootstrap.com/docs/4.0/assets/brand/bootstrap-social.png">
  <meta property="og:image:secure_url" content="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-social.png">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="1200">
  <meta property="og:image:height" content="630">

  
  <!--===============================================================================================-->  
  
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href=" {{asset('magnews/vendor/bootstrap/css/bootstrap.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('magnews/fonts/fontawesome-5.0.8/css/fontawesome-all.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('magnews/fonts/iconic/css/material-design-iconic-font.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('magnews/vendor/animate/animate.css')}}">
  <!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="{{asset('magnews/vendor/css-hamburgers/hamburgers.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('magnews/vendor/animsition/css/animsition.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('magnews/css/util.min.css')}}">
  <!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="{{asset('magnews/css/main.css')}}">
  <!--===============================================================================================-->

  <link rel="icon" href="{{ URL::asset('webmag/img/purbalinggaku1.png') }}" type="image/x-icon"/>

  <!-- Google font -->
  

  <!-- Bootstrap -->
  <link type="text/css" rel="stylesheet" href="{{ asset('webmag/css/bootstrap.min.css') }}"/>

  <!-- Font Awesome Icon -->
  <link rel="stylesheet" href="{{ asset('webmag/css/font-awesome.min.css') }}">

  <!-- Custom stlylesheet -->
  <link type="text/css" rel="stylesheet" href="{{ asset('webmag/css/style.css') }}"/>

  

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="animsition">

    <!-- Header -->
    <header>
      <!-- Header desktop -->
      <div class="container-menu-desktop">
        <div class="topbar">
          <div class="content-topbar container h-100">
            <div class="left-topbar">
              <span class="left-topbar-item flex-wr-s-c">
                <span>
                  Purbalingga
                </span>



                <span>

                </span>
              </span>

              <a href="#" class="left-topbar-item">
                About
              </a>

              <a href="#" class="left-topbar-item">
                Contact
              </a>

              <a href="#" class="left-topbar-item">
                Sing up
              </a>

              <a href="#" class="left-topbar-item">
                Log in
              </a>
            </div>

            <div class="right-topbar">
              <a href="#">
                <span class="fab fa-facebook-f"></span>
              </a>

              <a href="#">
                <span class="fab fa-twitter"></span>
              </a>

              <a href="#">
                <span class="fab fa-pinterest-p"></span>
              </a>

              <a href="#">
                <span class="fab fa-vimeo-v"></span>
              </a>

              <a href="#">
                <span class="fab fa-youtube"></span>
              </a>
            </div>
          </div>
        </div>

        <!-- Header Mobile -->
        <div class="wrap-header-mobile">
          <!-- Logo moblie -->    
          <div class="logo-mobile">
            <a href="index.html"><img src="{{asset('vendor/images/purbalinggaku01.png')}}" alt="IMG-LOGO"></a>
          </div>

          <!-- Button show menu -->
          <div class="btn-show-menu-mobile hamburger hamburger--squeeze m-r--8">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </div>
        </div>

        <!-- Menu Mobile -->
        <div class="menu-mobile">
          <ul class="topbar-mobile">
            <li class="left-topbar">
              <span class="left-topbar-item flex-wr-s-c">
                <span>
                  Purbalinggaku
                </span>

                <img class="m-b-1 m-rl-8" src="{{asset('vendor/images/icons/icon-night.png')}}" alt="IMG">

                <span>
                </span>
              </span>
            </li>

            <li class="left-topbar">
              <a href="#" class="left-topbar-item">
                About
              </a>

              <a href="#" class="left-topbar-item">
                Contact
              </a>

              <a href="#" class="left-topbar-item">
                Sing up
              </a>

              <a href="#" class="left-topbar-item">
                Log in
              </a>
            </li>

            <li class="right-topbar">
              <a href="#">
                <span class="fab fa-facebook-f"></span>
              </a>

              <a href="#">
                <span class="fab fa-twitter"></span>
              </a>

              <a href="#">
                <span class="fab fa-pinterest-p"></span>
              </a>

              <a href="#">
                <span class="fab fa-vimeo-v"></span>
              </a>

              <a href="#">
                <span class="fab fa-youtube"></span>
              </a>
            </li>
          </ul>

          <ul class="main-menu-m" >
            <?php
            $kategoris = \DB::table('kategori')->get();
            ?>
            @foreach($kategoris as $kt)
            <li><a href="{{ url('kategori/'.$kt->id==0) }}">{{ $kt->nama }}</a></li>
            @endforeach

            <span class="arrow-main-menu-m">
              <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
          </li>
        </ul>
      </div>

      <!--  -->
      <div class="wrap-logo container">
        <!-- Logo desktop -->   
        <div class="logo">
          <a href="index.html"><img src="{{asset('vendor/images/purbalinggaku01.png')}}" alt="LOGO"></a>
        </div>  

        <!-- Banner -->
        <div class="banner-header">
          <a href="#"><img src="images/banner-01.jpg" alt="IMG"></a>
        </div>
      </div>  

      <!--  -->
      <div class="wrap-main-nav">
        <div class="nav-menu nav navbar-nav">
          <!-- Menu desktop -->
          <div class="container">
            <nav class="menu-desktop">

              <ul class="main-menu">
                <?php
                $kategoris = \DB::table('kategori')->get();
                ?>
                @foreach($kategoris as $kt)
                <li><a href="{{ url('kategori/'.$kt->id==0) }}">{{ $kt->nama }}</a></li>
                @endforeach




              </ul>
            </nav>
          </div>
        </div>  
      </div>
    </header>

    <div class="container">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16f6ca8047f%20text%20%7B%20fill%3A%23333%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16f6ca8047f%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23555%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22276.9921875%22%20y%3D%22218%22%3EThird%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16f6ca8047f%20text%20%7B%20fill%3A%23333%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16f6ca8047f%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23555%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22276.9921875%22%20y%3D%22218%22%3EThird%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16f6ca80458%20text%20%7B%20fill%3A%23444%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16f6ca80458%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23666%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22247.3125%22%20y%3D%22218%22%3ESecond%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

    </div>

    <!-- /Header -->

    <!-- section -->
    <!-- /section -->

    <!-- section -->

    <!-- /section -->

    <!-- section -->
    @yield('content')
    <!-- /section -->

    <!-- Footer -->
    <footer id="footer">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row">
          <div class="col-md-5">
            <div class="footer-widget">
              <div class="footer-logo">
                <a href="index.html" class="logo"><img src="{{ asset('vendor/images/purbalinggaku01.png') }}" alt=""></a>
              </div>
              <ul class="footer-nav">
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Advertisement</a></li>
              </ul>
              <div class="footer-copyright">
                <span>&copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </span>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="row">
              <div class="col-md-6">
                <div class="footer-widget">
                  <h3 class="footer-title">About Us</h3>
                  <ul class="footer-links">
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="#">Join Us</a></li>
                    <li><a href="contact.html">Contacts</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-6">
                <div class="footer-widget">
                  <h3 class="footer-title">Catagories</h3>
                  <ul class="footer-links">
                    <li><a href="category.html">Web Design</a></li>
                    <li><a href="category.html">JavaScript</a></li>
                    <li><a href="category.html">Css</a></li>
                    <li><a href="category.html">Jquery</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="footer-widget">
              <h3 class="footer-title">Join our Newsletter</h3>
              <div class="footer-newsletter">
                <form>
                  <input class="input" type="email" name="newsletter" placeholder="Enter your email">
                  <button class="newsletter-btn"><i class="fa fa-paper-plane"></i></button>
                </form>
              </div>
              <ul class="footer-social">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
              </ul>
            </div>
          </div>

        </div>
        <!-- /row -->
      </div>
      <!-- /container -->
    </footer>
    <!-- /Footer -->

    <!-- jQuery Plugins -->


    

    <!--===============================================================================================-->  
    <script src="{{ asset('magnews/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('magnews/vendor/animsition/js/animsition.min.js') }}"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('magnews/bootstrap/js/popper.js') }}"></script>
    <script src="{{ asset('magnews/bootstrap/js/bootstrap.min.js') }}"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('magnews/js/main.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="/docs/4.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/docs/4.0/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="/docs/4.0/assets/js/vendor/popper.min.js"></script>
    

  </body>
  </html>
