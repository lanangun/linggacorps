<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Beranda_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artikel = \DB::table('artikels as a')->join('kategoris as k','k.id','=','a.id_kategori')->select('a.judul','a.isi','a.gambar','a.created_at','k.nama as kategori','a.artikel_id')->orderby('created_at','desc')->paginate(1);
        $random = \DB::table('artikels as a')->join('kategoris as k','k.id','=','a.id_kategori')->join('users as u','a.user_id','=','u.id')->select('a.judul','a.isi','a.gambar','a.created_at','k.nama as kategori','a.artikel_id')->limit(3)->inRandomOrder()->get();
        $slider = \DB::table('artikels as a')->join('kategoris as k','k.id','=','a.id_kategori')->select('a.judul','a.isi','a.gambar','a.created_at','k.nama as kategori','a.artikel_id')->orderby('created_at','desc')->paginate(1);
        $slider1 = \DB::table('artikels as a')->join('kategoris as k','k.id','=','a.id_kategori')->select('a.judul','a.isi','a.gambar','a.created_at','k.nama as kategori','a.artikel_id')->orderby('created_at','desc')->paginate(1);
        $slider2 = \DB::table('artikels as a')->join('kategoris as k','k.id','=','a.id_kategori')->select('a.judul','a.isi','a.gambar','a.created_at','k.nama as kategori','a.artikel_id')->orderby('created_at','desc')->paginate(1);
        return view('welcome',compact('artikel','random','slider','slider1','slider2'));
    }

    public function search(Request $request){
        $search = $request->search;
        $artikel = \DB::table('artikels as a')->join('kategoris as k','k.id','=','a.id_kategori')->where('a.judul','like','%'.$search.'%')->orWhere(
            'a.isi','like','%'.$search.'%')->select('a.judul','a.isi','a.gambar','a.created_at','k.nama as kategori','a.artikel_id')->orderby('created_at','desc')->paginate(1);

        return view('welcome',compact('artikel'));
    }

    public function detail($artikel_id){
    	$artikel = \DB::table('artikels as a')->join('users as u','a.user_id','=','u.id')->where('a.artikel_id',$artikel_id)->first();
    	

    	return view('detail',compact('artikel'));

    
    }
}
