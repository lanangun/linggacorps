<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentars', function (Blueprint $table) {
          $table->increments('komentar_id');
            $table->integer('artikel_id');
            $table->string('nama',115);
            $table->string('email',115);
            $table->string('website',115);
            $table->text('isi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('komentar', function (Blueprint $table) {
          //
      });
    }
}
