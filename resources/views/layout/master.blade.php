<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Purbalinggaku</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link rel="icon" href="{{ URL::asset('webmag/img/purbalinggaku1.png') }}" type="image/x-icon"/>
 

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{asset('bizpage/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{asset('bizpage/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('bizpage/lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('bizpage/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('bizpage/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('bizpage/lib/lightbox/css/lightbox.min.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{asset('bizpage/css/style.css')}}" rel="stylesheet">

  
>

  
  </head>

  <body>

  <!--==========================
    Header
    ============================-->
    <header id="header">
      <div class="container-fluid">

        <div id="logo" class="pull-left">
          <div class="column">
            <h1><a href="http://localhost/purbacorps/public/" class="scrollto">Purbalinggaku</a></h1>
          </div>
        </div>

        <nav id="nav-menu-container">
          <ul class="nav-menu ">
            <li class="menu-active"><a href="#intro">Beranda</a></li>
            <li class="menu-has-children"><a href="">Kategori</a>
              <ul>
                <?php
                $kategoris = \DB::table('kategoris')->get();
                ?>
                @foreach($kategoris as $kt)
                <li><a href="{{ url('artikel/kategori/'.$kt->id) }}">{{ $kt->nama }}</a></li>
                @endforeach
              </ul>
            </li>
            <li><form class="form-inline mt-2 mt-md-0" method="get" action="{{ url('search') }}">
              <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
              <button class="submit" style="color:#18d26e">Search</button>
            </form></li>
          </ul>
        </nav><!-- #nav-menu-container -->
      </div>
    </header>
    <!-- #header -->

  <!--==========================
    Intro Section
    ============================-->
    

    @yield('content')

    <main id="main">

    </main>

  <!--==========================
    Footer
    ============================-->
    <footer id="footer">
      <div class="footer-top">
        <div class="container">
          <div class="row">

            <div class="col-lg-3 col-md-6 footer-info">
              <h4>Purbalinggaku</h4>
              <p>Purbalinggaku adalah portal informasi masyarakat Purbalingga dan sekitarnya</p>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
              <h4>Menu</h4>
              <ul>
              <li class="menu-active"><a href="">Beranda</a></li>
            <li class="menu-has-children"><a href="">Kategori</a>
              
            </li>
                
                
              </ul>
            </div>

            <div class="col-lg-3 col-md-6 footer-contact">
              <h4>Sosial Media</h4>
              
              <div class="social-links">
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
              </div>

            </div>

            

      <div class="container">
        <div class="copyright">
          &copy; Copyright <strong>BizPage</strong>. All Rights Reserved
        </div>
        <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        -->
        Best <a href="https://bootstrapmade.com/">Bootstrap Templates</a> by BootstrapMade
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="{{asset('bizpage/lib/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/easing/easing.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/superfish/hoverIntent.js')}}"></script>
  <script src="{{asset('bizpage/lib/superfish/superfish.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/wow/wow.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/waypoints/waypoints.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/counterup/counterup.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/isotope/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/lightbox/js/lightbox.min.js')}}"></script>
  <script src="{{asset('bizpage/lib/touchSwipe/jquery.touchSwipe.min.js')}}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{asset('bizpage/contactform/contactform.js')}}"></script>

 

  <!-- Template Main Javascript File -->
  <script src="{{asset('bizpage/js/main.js')}}"></script>
  <script type="text/javascript">
    .row {
      display: flex;
      flex-wrap: wrap;
      padding: 0 4px;
    }

    /* Create four equal columns that sits next to each other */
    .column {
      flex: 25%;
      max-width: 25%;
      padding: 0 4px;
    }

    .column img {
      margin-top: 8px;
      vertical-align: middle;
      width: 100%;
    }

    /* Responsive layout - makes a two column-layout instead of four columns */
    @media screen and (max-width: 800px) {
      .column {
        flex: 50%;
        max-width: 50%;
      }
    }

    /* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
    @media screen and (max-width: 600px) {
      .column {
        flex: 100%;
        max-width: 100%;
      }
    }
  </script>

</body>
</html>
