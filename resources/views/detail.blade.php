@extends('layout.master')

@section('content')

<div class="section">
	<div class="container">
		<div class="row">
			
			<div class="col-md-8">
				<div class="section-row sticky-container">
					<div class="main-post">
						<h3>{{ $artikel->judul }}</h3>

						<p>
							<center>
								<img style="width: 50%;" src="{{ asset('uploads/'.$artikel->gambar) }}">
							</center>
						</p>

						<p>
							{!! $artikel->isi !!}
						</p>
					</div>
					<div class="post-shares sticky-shares" style="position: absolute; top: 0px; left: 0px;">
						<a href="#" class="share-facebook"><i class="fa fa-facebook"></i></a>
						<a href="#" class="share-twitter"><i class="fa fa-twitter"></i></a>
						<a href="#" class="share-google-plus"><i class="fa fa-google-plus"></i></a>
						<a href="#" class="share-pinterest"><i class="fa fa-pinterest"></i></a>
						<a href="#" class="share-linkedin"><i class="fa fa-linkedin"></i></a>
						<a href="#"><i class="fa fa-envelope"></i></a>
					</div>
				</div>

				<!-- ad -->
				<div class="section-row text-center">
					<a href="#" style="display: inline-block;margin: auto;">
						<img class="img-responsive" src="./img/ad-2.jpg" alt="">
					</a>
				</div>
				<!-- ad -->

				<!-- author -->
				<div class="section-row">
					<div class="post-author">
						<div class="media">
							<div class="media-left">
								<img class="media-object" src="./img/author.png" alt="">
							</div>
							<div class="media-body">
								<div class="media-heading">
									<h3>{{ $artikel->name }}</h3>
								</div>
								<div class="social-links">
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
              </div>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- /author -->

				
					</div>
				</div>
				<!-- /comments -->

				<!-- reply -->
				
			</div>

		</div>
	</div>
</div>

@endsection