<!DOCTYPE html>
<html lang="en">
<head>
	<title>Purbalinggaku</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script src="{{ asset('webmag/js/jquery.min.js') }}"></script>
	<script src="{{ asset('webmag/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('webmag/js/main.js') }}"></script>
</head>
<body>
 
   <div class="container">		
	<center><h1></h1></center>
	<br/>
	<div class="col-md-8 col-md-offset-2">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>		
			</ol>
 
			<!-- deklarasi carousel -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<div class="carousel-caption">
						<ul class="nav-menu nav navbar-nav">
							<?php
								$kategoris = \DB::table('kategori')->get();
							?>
							@foreach($kategoris as $kt)
							<li><a href="{{ url('artikel/kategori/'.$kt->id) }}">{{ $kt->nama }}</a></li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="item">
					<img src="img/malasngoding2.png" alt="www.malasngoding.com">
					<div class="carousel-caption">
						<h3>Tutorial Bootstrap</h3>
						<p>Belajar tutorial bootstrap dasar di www.malasngoding.com</p>
					</div>
				</div>
				<div class="item">
					<img src="img/malasngoding3.png" alt="www.malasngoding.com">
					<div class="carousel-caption">
						<h3>Tutorial Android</h3>
						<p>Tutorial membuat aplikasi android.</p>
					</div>
				</div>				
			</div>
 
			<!-- membuat panah next dan previous -->
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
   </div>
</body>
</html>