<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Kategori_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $title = 'Manage Kategori';
    $kategori = \DB::table('kategoris as k')->join('users as u','k.user_id','=','u.id')->select('k.nama','u.name','k.created_at','k.id')->get();
    return view('admin.kategori.kategori_index', compact('title', 'kategori'));
    }

    public function add(){
    $title = 'Tambah Kategori';

    return view('admin.kategori.kategori_add',compact('title'));
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
      'nama'=>'required'
    ]);

    $nama = $request->nama;

    	\DB::table('kategoris')->insert([
    		'nama'=>$nama,
    		'user_id'=>\Auth::user()->id,
    		'created_at'=>date('Y-m-d H:i:s'),
    		'updated_at'=>date('Y-m-d H:i:s')
    	]);

    	return redirect('admin/kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $title = 'Edit Kategori';
      $kategori = \DB::table('kategoris')->where('id',$id)->first();

      return view('admin.kategori.kategori_edit',compact('kategori','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      \DB::table('kategoris')->where('id',$id)->update([
        'nama'=>$request->nama,
        'updated_at'=>date('Y-m-d H:i:s')
    ]);

    return redirect('admin/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
      \DB::table('kategoris')->where('id',$id)->delete();

      return redirect('admin/kategori');
    }
}
