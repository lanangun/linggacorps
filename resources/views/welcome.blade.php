@extends('layout.master')

@section('content')
<!--==========================
    Intro Section
   
    ============================-->
    <section id="intro">
      <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

          <ol class="carousel-indicators"></ol>

          <div class="carousel-inner" role="listbox">
            
          @foreach($artikel as $at)
            <div class="carousel-item active" style="background-image: url({{asset('uploads/'.$at->gambar)}});">
            
              <div class="carousel-container">
                <div class="carousel-content">
                  <h3></h3>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  <a href="#featured-services" class="btn-get-started scrollto">{{ $at->judul }}</a>
                </div>
              </div>
            </div>
            @endforeach
            @foreach($slider as $sd)
            <div class="carousel-item" style="background-image: url({{asset('uploads/'.$sd->gambar)}});">
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2></h2>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  <a href="#featured-services" class="btn-get-started scrollto">{{ $sd->judul }}</a>
                </div>
              </div>
            </div>
            @endforeach
            @foreach($slider1 as $sd1)
             <div class="carousel-item" style="background-image: url({{asset('uploads/'.$sd1->gambar)}});">
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2></h2>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  <a href="#featured-services" class="btn-get-started scrollto">{{ $sd1->judul }}</a>
                </div>
              </div>
            </div>
            @endforeach
            @foreach($slider2 as $sd2)
            <div class="carousel-item" style="background-image: url({{asset('uploads/'.$sd2->gambar)}});">
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2></h2>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  <a href="#featured-services" class="btn-get-started scrollto">{{ $sd2->judul }}</a>
                </div>
              </div>
            </div>

            <div class="carousel-item" style="background-image: url({{asset('uploads/'.$at->gambar)}});">
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2></h2>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  <a href="#featured-services" class="btn-get-started scrollto">{{ $at->judul }}</a>
                </div>
              </div>
            </div>
            @endforeach

          <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>

          <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>

        </div>
      </div>
    </section><!-- #intro -->
     <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 box">
          </div>

          <div class="col-lg-4 box">
          </div>

          <div class="col-lg-4 box">
            </div>

        </div>
      </div>
    </section><!-- #featured-services -->
	 <div class="container">

        <header class="section-header">
          <h3>Berita Utama</h3>
        </header>

         <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">
      <div class="row about-cols">
      @if(isset($random))
			@foreach($random as $rm)
          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
          <div class="post post-thumb">
            <div class="about-col">
              <div class="img">
                <img src="{{ asset('uploads/'.$rm->gambar) }}" alt="" class="img-fluid">
              </div>
              <h2 class="title"><a href="#">{{ $rm->judul }}</a></h2>
            </div>
            </div>
          </div>
        
        @endforeach
      @endif
      </div>
      </div>
    </section><!-- #about -->

<div class="section">
	<!-- container -->
	<div class="container">

		<div class="row">	
			<!-- post -->
			@if(isset($random))
			@foreach($random as $rm)
			<div class="col-md-8">
				<div class="post post-thumb">
					<a class="post-img" href="{{ url('detail/'.$rm->artikel_id) }}"><img style="height: 400px" src="{{ asset('uploads/'.$rm->gambar) }}" alt=""></a>
					<div class="post-body">
						<div class="post-meta">
							<a class="post-category cat-2" href="category.html">{{ $rm->kategori }}</a>
							<span class="post-date">{{ date('d M Y',strtotime($rm->created_at)) }}</span>
						</div>
						<h3 class="post-title"><a href="{{ url('detail/'.$rm->artikel_id) }}">{{ $rm->judul }}</a></h3>
					</div>
				</div>
			</div>
			@endforeach
			@endif
			<!-- /post -->
		</div>
		<!-- row -->
		<div class="row">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2>Recent Posts</h2>
						</div>
					</div>
					<!-- post -->

					
					
					<!-- /post -->

					<div class="col-md-12">
						<div class="section-row">
							<button class="primary-button center-block">Load More</button>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				

				

				<!-- tags -->
				<div class="aside-widget">
					<div class="tags-widget">
						<ul>
							
						</ul>
					</div>
				</div>
				<!-- /tags -->
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>

<!-- Feature post -->
<section class="bg0">
		<div class="container">
			<div class="row m-rl--1">
      @if(isset($random))
			@foreach($random as $rm)
				<div class="col-md-6 p-rl-1 p-b-2">
					<div class="bg-img1 size-a-3 how1 pos-relative" style="background-image: url({{asset('uploads/'.$rm->artikel_id)}});">
						<a href="{{ url('detail/'.$rm->artikel_id) }}" class="dis-block how1-child1 trans-03"></a>

						<div class="flex-col-e-s s-full p-rl-25 p-tb-20">
							<a href="#" class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
							{{ $rm->kategori }}
							</a>

							<h3 class="how1-child2 m-t-14 m-b-10">
								<a href="{{ url('detail/'.$rm->artikel_id) }}" class="how-txt1 size-a-6 f1-l-1 cl0 hov-cl10 trans-03">
                {{ $rm->judul }}
								</a>
							</h3>

							<span class="how1-child2">
								<span class="f1-s-4 cl11">
									Jack Sims
								</span>

								<span class="f1-s-3 cl11 m-rl-3">
									-
								</span>

								<span class="f1-s-3 cl11">
									Feb 16
								</span>
							</span>
						</div>
					</div>
				</div>

				<div class="col-md-6 p-rl-1">
					<div class="row m-rl--1">
						<div class="col-12 p-rl-1 p-b-2">
							<div class="bg-img1 size-a-4 how1 pos-relative" style="background-image: url(images/post-02.jpg);">
								<a href="{{ url('detail/'.$rm->artikel_id) }}" class="dis-block how1-child1 trans-03"></a>

								<div class="flex-col-e-s s-full p-rl-25 p-tb-24">
									<a href="#" class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
										Culture
									</a>

									<h3 class="how1-child2 m-t-14">
										<a href="{{ url('detail/'.$rm->artikel_id) }}" class="how-txt1 size-a-7 f1-l-2 cl0 hov-cl10 trans-03">
											London ipsum dolor sit amet, consectetur adipiscing elit.
										</a>
									</h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 p-rl-1 p-b-2">
							<div class="bg-img1 size-a-5 how1 pos-relative" style="background-image: url(images/post-03.jpg);">
								<a href="{{ url('detail/'.$rm->artikel_id) }}" class="dis-block how1-child1 trans-03"></a>

								<div class="flex-col-e-s s-full p-rl-25 p-tb-20">
									<a href="#" class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
										Life Style
									</a>

									<h3 class="how1-child2 m-t-14">
										<a href="{{ url('detail/'.$rm->artikel_id) }}" class="how-txt1 size-h-1 f1-m-1 cl0 hov-cl10 trans-03">
											Pellentesque dui nibh, pellen-tesque ut dapibus ut
										</a>
									</h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 p-rl-1 p-b-2">
							<div class="bg-img1 size-a-5 how1 pos-relative" style="background-image: url(images/post-04.jpg);">
								<a href="{{ url('detail/'.$rm->artikel_id) }}" class="dis-block how1-child1 trans-03"></a>

								<div class="flex-col-e-s s-full p-rl-25 p-tb-20">
									<a href="#" class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
										Sport
									</a>

									<h3 class="how1-child2 m-t-14">
										<a href="{{ url('detail/'.$rm->artikel_id) }}" class="how-txt1 size-h-1 f1-m-1 cl0 hov-cl10 trans-03">
											Motobike Vestibulum vene-natis purus nec nibh volutpat
										</a>
									</h3>
								</div>
							</div>
						</div>
					</div>
        </div>
        @endforeach
			@endif
      </div>
      
		</div>
	</section>

	<!-- Post -->

@endsection
